See readme and issues on [GitHub](https://github.com/probins/oles) for general information/description.

### This repo
contains:
* `jscodeshift` code-mods in `transforms/` (modified versions of OL's `module.js`):
  * `browsermodule.js` for transforming  OL source files into browser-compatible ES modules (reports error for typedefs.js which can be ignored); uses the [ES-module version](https://github.com/probins/olextes) of external code
  * `examples.js` for transforming OL examples to import from the OL modules
* a simple generic `example.html` for calling these examples
* a script `createIndex.js` for creating a very basic index of examples
* a `.babelrc` for use with `babel-cli`/`babel-minify`
* a `.gitlab-ci.yml` which on every push to gitlab (or manual start) creates GitLab Pages:
  * copies OL source files and transforms to browser-compatible ES modules in `lib/`
  * minifies the modules using `babel-minify` into `dist/`
  * copies OL example JS files into `examples`, and transforms so they import from ES modules
  * copies over `example.html`
  * uses `createIndex.js` to create basic list of example source files in `examples/index.html`
  * deploys to GitLab Pages

The source can then be viewed at (and, of course, imported from) for example https://probins.gitlab.io/oles/lib/extent.js or https://probins.gitlab.io/oles/dist/extent.js.

`example.html` is available at https://probins.gitlab.io/oles/examples/example.html (call with '?examplename'; default is '?simple'), and the basic list of example source files at https://probins.gitlab.io/oles/examples/index.html. Not all examples work in `example.html`, as some need special resources or DOM elements. These examples currently all use `Map`, so load a large amount of unneeded code. Most of them should be changed to use `PluggableMap` and other base classes; see Github issues for further discussion. [OL ES examples repo](https://github.com/probins/olexamples) contains examples of using `PluggableMap`.

The output of `git describe --tags` is available at https://probins.gitlab.io/oles/describe, specifying which OL version this corresponds to. The script uses my fork of OL master, which is updated whenever I get around to it; it is not necessarily the latest OL master. ATM it is started manually, though it could be changed to be triggered whenever a commit is merged into OL master.

### Contributions
are welcome, as are all good ideas on how to take this further.
