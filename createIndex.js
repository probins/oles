const fs = require('fs');

let op = `<!DOCTYPE html>
`;

fs.readdirSync('moutput/examples').forEach(file => {
  if (file.includes('.js')) {
    op += `<a href="${file}">${file.replace('.js', '')}<a>
`;
  }
});

fs.writeFile('moutput/examples/index.html', op, function(err) {
    if(err) {
      console.log(err);
    } else {
      console.log("index file created");
    }
}); 
